<?php

/**
 * @file
 * course_zoom.features.inc
 */

/**
 * Implements hook_views_api().
 */
function course_zoom_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
