<?php

/**
 * @file
 * course_zoom.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function course_zoom_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'course_zoom_object_report';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'course_outline_fulfillment';
  $view->human_name = 'course_zoom_object_report';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Object report';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'uid' => 'uid',
    'field_first_name' => 'field_first_name',
    'field_last_name' => 'field_last_name',
    'date_started' => 'date_started',
    'date_completed' => 'date_completed',
    'grade_result' => 'grade_result',
    'nothing' => 'nothing',
  );
  $handler->display->display_options['style_options']['default'] = 'date_completed';
  $handler->display->display_options['style_options']['info'] = array(
    'uid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_first_name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_last_name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'date_started' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'date_completed' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'grade_result' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Footer: Global: Result summary */
  $handler->display->display_options['footer']['result']['id'] = 'result';
  $handler->display->display_options['footer']['result']['table'] = 'views';
  $handler->display->display_options['footer']['result']['field'] = 'result';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'There are no attempts on this object.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  /* Relationship: Course object fulfillment: Uid */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'course_outline_fulfillment';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  /* Relationship: User: Profile */
  $handler->display->display_options['relationships']['profile']['id'] = 'profile';
  $handler->display->display_options['relationships']['profile']['table'] = 'users';
  $handler->display->display_options['relationships']['profile']['field'] = 'profile';
  $handler->display->display_options['relationships']['profile']['relationship'] = 'uid';
  $handler->display->display_options['relationships']['profile']['bundle_types'] = array(
    'profile' => 'profile',
  );
  /* Field: Course object fulfillment: Uid */
  $handler->display->display_options['fields']['uid']['id'] = 'uid';
  $handler->display->display_options['fields']['uid']['table'] = 'course_outline_fulfillment';
  $handler->display->display_options['fields']['uid']['field'] = 'uid';
  $handler->display->display_options['fields']['uid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['uid']['separator'] = '';
  /* Field: Profile: First name */
  $handler->display->display_options['fields']['field_first_name']['id'] = 'field_first_name';
  $handler->display->display_options['fields']['field_first_name']['table'] = 'field_data_field_first_name';
  $handler->display->display_options['fields']['field_first_name']['field'] = 'field_first_name';
  $handler->display->display_options['fields']['field_first_name']['relationship'] = 'profile';
  $handler->display->display_options['fields']['field_first_name']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_first_name']['alter']['path'] = 'user/[uid]';
  /* Field: Profile: Last name */
  $handler->display->display_options['fields']['field_last_name']['id'] = 'field_last_name';
  $handler->display->display_options['fields']['field_last_name']['table'] = 'field_data_field_last_name';
  $handler->display->display_options['fields']['field_last_name']['field'] = 'field_last_name';
  $handler->display->display_options['fields']['field_last_name']['relationship'] = 'profile';
  $handler->display->display_options['fields']['field_last_name']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_last_name']['alter']['path'] = 'user/[uid]';
  /* Field: User: E-mail */
  $handler->display->display_options['fields']['mail']['id'] = 'mail';
  $handler->display->display_options['fields']['mail']['table'] = 'users';
  $handler->display->display_options['fields']['mail']['field'] = 'mail';
  $handler->display->display_options['fields']['mail']['relationship'] = 'uid';
  $handler->display->display_options['fields']['mail']['label'] = 'Email';
  /* Field: Course object fulfillment: Date_started */
  $handler->display->display_options['fields']['date_started']['id'] = 'date_started';
  $handler->display->display_options['fields']['date_started']['table'] = 'course_outline_fulfillment';
  $handler->display->display_options['fields']['date_started']['field'] = 'date_started';
  $handler->display->display_options['fields']['date_started']['label'] = 'Started';
  $handler->display->display_options['fields']['date_started']['date_format'] = 'short';
  $handler->display->display_options['fields']['date_started']['second_date_format'] = 'long';
  /* Field: Course object fulfillment: Date_completed */
  $handler->display->display_options['fields']['date_completed']['id'] = 'date_completed';
  $handler->display->display_options['fields']['date_completed']['table'] = 'course_outline_fulfillment';
  $handler->display->display_options['fields']['date_completed']['field'] = 'date_completed';
  $handler->display->display_options['fields']['date_completed']['label'] = 'Completed';
  $handler->display->display_options['fields']['date_completed']['date_format'] = 'short';
  $handler->display->display_options['fields']['date_completed']['second_date_format'] = 'long';
  /* Field: Course object fulfillment: Info */
  $handler->display->display_options['fields']['info']['id'] = 'info';
  $handler->display->display_options['fields']['info']['table'] = 'course_outline_fulfillment';
  $handler->display->display_options['fields']['info']['field'] = 'info';
  $handler->display->display_options['fields']['info']['label'] = 'Attendance time (minutes)';
  $handler->display->display_options['fields']['info']['format'] = 'key';
  $handler->display->display_options['fields']['info']['key'] = 'time_spent_minutes';
  /* Contextual filter: Global: Null */
  $handler->display->display_options['arguments']['null']['id'] = 'null';
  $handler->display->display_options['arguments']['null']['table'] = 'views';
  $handler->display->display_options['arguments']['null']['field'] = 'null';
  $handler->display->display_options['arguments']['null']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['null']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['null']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['null']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['null']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['null']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['null']['validate']['type'] = 'course';
  $handler->display->display_options['arguments']['null']['validate_options']['access'] = TRUE;
  $handler->display->display_options['arguments']['null']['validate_options']['access_op'] = 'update';
  /* Contextual filter: Course object fulfillment: Coid */
  $handler->display->display_options['arguments']['coid']['id'] = 'coid';
  $handler->display->display_options['arguments']['coid']['table'] = 'course_outline_fulfillment';
  $handler->display->display_options['arguments']['coid']['field'] = 'coid';
  $handler->display->display_options['arguments']['coid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['coid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['coid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['coid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Realname: Name */
  $handler->display->display_options['filters']['realname']['id'] = 'realname';
  $handler->display->display_options['filters']['realname']['table'] = 'realname';
  $handler->display->display_options['filters']['realname']['field'] = 'realname';
  $handler->display->display_options['filters']['realname']['relationship'] = 'uid';
  $handler->display->display_options['filters']['realname']['operator'] = 'contains';
  $handler->display->display_options['filters']['realname']['exposed'] = TRUE;
  $handler->display->display_options['filters']['realname']['expose']['operator_id'] = 'realname_op';
  $handler->display->display_options['filters']['realname']['expose']['label'] = 'Name';
  $handler->display->display_options['filters']['realname']['expose']['operator'] = 'realname_op';
  $handler->display->display_options['filters']['realname']['expose']['identifier'] = 'realname';
  $handler->display->display_options['filters']['realname']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    96518088 => 0,
    175031666 => 0,
    36427370 => 0,
    108600946 => 0,
    92961712 => 0,
    21091174 => 0,
    26348183 => 0,
    137264725 => 0,
    137756511 => 0,
    223931359 => 0,
  );
  $handler->display->display_options['filters']['realname']['expose']['autocomplete_items'] = '10';
  $handler->display->display_options['filters']['realname']['expose']['autocomplete_min_chars'] = '0';
  $handler->display->display_options['filters']['realname']['expose']['autocomplete_raw_suggestion'] = 1;
  $handler->display->display_options['filters']['realname']['expose']['autocomplete_raw_dropdown'] = 1;
  $handler->display->display_options['filters']['realname']['expose']['autocomplete_dependent'] = 0;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'node/%/course-reports/objects/%/overview';

  /* Display: Data export */
  $handler = $view->new_display('views_data_export', 'Data export', 'views_data_export_1');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_data_export_csv';
  $handler->display->display_options['path'] = 'node/%/course-reports/objects/%/zoom.csv';
  $handler->display->display_options['displays'] = array(
    'page' => 'page',
    'default' => 0,
  );
  $export['course_zoom_object_report'] = $view;

  return $export;
}
