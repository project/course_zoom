<?php

use Firebase\JWT\JWT;

class Zoom {

  /**
   * Retrieves Zoom API key and secret set globally. Using these inputs, a JWT
   * is built and returned.
   *
   * @return string $jwt
   *   Returns a JWT encoded with user values retrieved from their Zoom
   *   account.
   */
  function auth() {
    $key = variable_get('course_zoom_api_key');
    $secret = variable_get('course_zoom_api_secret');

    $token = [
      'iss' => $key,
      'exp' => time() + 60
    ];

    $jwt = JWT::encode($token, $secret);
    return $jwt;
  }

  /**
   * Call a Zoom API method.
   *
   * @param string $method
   *   Type of HTTP call.
   * @param string $endpoint
   *   The Zoom resource.
   * @param array $parameters
   *    PHP array being converted to JSON by Guzzle and sent to Zoom.
   *
   * @return object $response_obj
   *      Returns the JSON decoded object returned from Zoom.
   */
  function call($method, $endpoint, $parameters = []) {
    $jwt = $this->auth();

    try {
      $client = new GuzzleHttp\Client();
      $response = $client->$method('https://api.zoom.us/v2/' . $endpoint, [
        'json' => $parameters,
        'headers' => [
          'Authorization' => 'Bearer ' . $jwt
        ]
      ]);
    } catch (Exception $e) {
      drupal_set_message(t('There was an error communicating with Zoom. Please contact your administrator.'), 'error');
      watchdog('course_zoom', $e->getMessage());
      return;
    }

    $response_obj = json_decode($response->getBody(), true);
    return $response_obj;
  }

  /**
   * @see Zoom::call()
   */
  function get($endpoint, $parameters = []) {
    return $this->call('get', $endpoint, $parameters);
  }

  /**
   * @see Zoom::call()
   */
  function post($endpoint, $parameters = []) {
    return $this->call('post', $endpoint, $parameters);
  }

  /**
   * @see Zoom::call()
   */
  function delete($endpoint, $parameters = []) {
    return $this->call('delete', $endpoint, $parameters);
  }

  /**
   * @see Zoom::call()
   */
  function patch($endpoint, $parameters = []) {
    return $this->call('patch', $endpoint, $parameters);
  }

  /**
   * @see Zoom::call()
   */
  function put($endpoint, $parameters = []) {
    return $this->call('put', $endpoint, $parameters);
  }

}
