<?php

class CourseObjectZoomFulfillment extends CourseObjectFulfillment {

  function optionsDefinition() {
    $definitions = parent::optionsDefinition();
    $definitions['time_spent'] = '';
    $definitions['time_spent_minutes'] = '';
    return $definitions;
  }

  /**
   * Remove user from Zoom meeting on fulfillment delete.
   *
   * {@inheritdoc}
   */
  public function delete() {
    $zoom = new Zoom();
    $id = $this->getCourseObject()->getInstanceId();
    $reg = (object) ['email' => user_load($this->uid)->mail];
    $payload = [
      'action' => 'cancel',
      'registrants' => [$reg],
    ];
    $mode = $this->getCourseObject()->getMode();
    $endpoint = "{$mode}s";
    $zoom->put("$endpoint/$id/registrants/status", $payload);
    return parent::delete();
  }

}
