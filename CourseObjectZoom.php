<?php

class CourseObjectZoom extends CourseObject {

  /**
   * Mode of this object.
   *
   * @return string
   *   'webinar' or 'meeting'
   */
  function getMode() {
    return $this->getOption('mode') ?: 'meeting';
  }

  /**
   * {@inheritdoc}
   */
  function getTakeType() {
    return 'content';
  }

  /**
   * {@inheritdoc}
   *
   * Retrieves zoom ID from local Zoom object and uses this to retrieve a Zoom
   * join URL for that meeting. This URL is returned to the user in a clickable
   * link.
   */
  function take() {
    global $user;

    $profile = profile2_load_by_user($user, 'profile');
    $p_wrap = entity_metadata_wrapper('profile2', $profile->pid);
    $first_name = $p_wrap->field_first_name->value();
    $last_name = $p_wrap->field_last_name->value();

    $id = $this->getInstanceId();

    if ($id == '') {
      return t('No Zoom meeting configured. Please check the course object settings.');
    }

    // Save the email in the fulfillment for easy lookups.
    $fulfillment = $this->getFulfillment($user);

    $options_arr = array(
      'email' => $user->mail,
      'first_name' => $first_name,
      'last_name' => $last_name,
    );

    $page = [];
    $helptext = check_markup($this->getOption('helptext_value'), $this->getOption('helptext_format'));
    $page['helptext']['#markup'] = $helptext;

    $zoomapi = new Zoom();
    $endpoint = "{$this->getMode()}s";
    $response_obj = $zoomapi->post($endpoint . '/' . $id . '/registrants', $options_arr);

    if ($response_obj && !$fulfillment->getOption('join_url')) {
      // Save the join URL in the fulfillment.
      $fulfillment->setOption('instance', $user->mail)
        ->setOption('join_url', $response_obj['join_url'])
        ->save();
    }

    $link = l(t('Join the @mode', ['@mode' => $this->getMode()]), $fulfillment->getOption('join_url'), ['attributes' => ['target' => '_blank']]);

    $page['topic']['#value'] = '<strong>' . check_plain($this->getOption('topic')) . '</strong>';
    $page['topic']['#theme'] = 'html_tag';
    $page['topic']['#tag'] = 'p';

    $page['description']['#value'] = check_plain($this->getOption('description'));
    $page['description']['#theme'] = 'html_tag';
    $page['description']['#tag'] = 'p';
    $page['description']['#prefix'] = '<strong>' . t('Description') . '</strong>';

    $page['start_date']['#value'] = format_date(strtotime($this->getOption('start_time')));
    $page['start_date']['#theme'] = 'html_tag';
    $page['start_date']['#prefix'] = '<strong>' . t('@mode time', ['@mode' => ucfirst($this->getMode())]) . '</strong>';
    $page['start_date']['#tag'] = 'p';

    $page['duration']['#value'] = $this->getOption('duration') . ' ' . t('minutes');
    $page['duration']['#prefix'] = '<strong>' . t('Duration') . '</strong>';
    $page['duration']['#theme'] = 'html_tag';
    $page['duration']['#tag'] = 'p';

    $page['link']['#value'] = $link;
    $page['link']['#theme'] = 'html_tag';
    $page['link']['#tag'] = 'p';

    $page['help']['#value'] = t('When joining the @mode, a new tab may be opened. After the @mode is over, close the tab to return to the course.', ['@mode' => $this->getMode()]);
    $page['help']['#theme'] = 'html_tag';
    $page['help']['#tag'] = 'p';

    return $page;
  }

  /**
   * {@inheritdoc}
   */
  public function optionsSubmit(&$form, &$form_state) {
    // Workaround for the options form not handling text_format very well.
    if (!isset($form_state['values']['helptext_value'])) {
      $form_state['values']['helptext_value'] = $form_state['values']['helptext']['value'];
      $form_state['values']['helptext_format'] = $form_state['values']['helptext']['format'];
    }
    parent::optionsSubmit($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function optionsValidate(&$form, &$form_state) {
    if (($form['zoom']['zoom_info']['zoom_id_fset']['instance']['#value'] == '') && ($form_state['values']['response'] != 'create')) {
      form_error($form['zoom']['zoom_info']['zoom_id_fset']['instance'], t('Meeting ID is required.'));
    }
    parent::optionsValidate($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getWarnings() {
    $warnings = parent::getWarnings();
    $options = $this->getOptions();

    if ($options['topic'] == '') {
      $this->getId();
      $destination = url('node/' . $this->getCourseNid() . '/course-outline');
      $link = l(t('configure the Zoom meeting'), "node/{$this->getCourseNid()}/course-object/nojs/{$this->getId()}/options", ['query' => ['destination' => $destination]]);
      $warnings[] = t('No meeting found. Please !link.', ['!link' => $link]);
    }
    return $warnings;
  }

  /**
   * {@inheritdoc}
   */
  public function optionsDefinition() {
    $options = parent::optionsDefinition();
    // Meeting default values
    $options['mode'] = 'meeting';
    $options['topic'] = '';
    $options['type'] = '';
    $options['description'] = '';
    $options['instructions'] = '';
    $options['start_time'] = '';
    $options['time_zone'] = drupal_get_user_timezone();
    $options['duration'] = '';
    $options['password'] = '';
    $options['response'] = 'default';
    $options['alternative_hosts'] = '';
    $options['audio'] = 'both';
    $options['participant'] = 'par_off';
    $options['host'] = 'host_off';
    $options['recording_options'] = 'cloud';
    $options['meeting_options'] = '';
    $options['required_attendance'] = '';
    $options['helptext_value'] = '';
    $options['helptext_format'] = '';
    $options['post_webinar_survey'] = FALSE;
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function optionsForm(&$form, &$form_state) {
    parent::optionsForm($form, $form_state);
    $userId = variable_get('course_zoom_host_account');

    if ($userId == '') {
      global $user;
      $userId = $user->mail;
    }

    $defaults = $this->getOptions();
    $zoomapi = new Zoom();
    $parameter = array('query' => ['page_size' => 300]);

    if (!empty($form_state['values']['mode'])) {
      $mode = $form_state['values']['mode'];
      $endpoint = "{$form_state['values']['mode']}s";
    }
    else {
      $mode = $this->getMode();
      $endpoint = "{$this->getMode()}s";
    }

    $response_obj = $zoomapi->get('users/' . $userId . '/' . $endpoint, $parameter);
    $meeting_obj_arr = $response_obj[$endpoint];
    $meeting_topic_id_arr[''] = 'Select an option';
    $meeting_topic_id_arr['create'] = t('Create new @mode', ['@mode' => $mode]);
    $meeting_topic_id_arr['existing_id'] = t('Use existing @mode ID', ['@mode' => $mode]);

    foreach ($meeting_obj_arr as $obj) {
      $meeting_topic_id_arr[$obj['id']] = $obj['topic'] . ', ' . $obj['id'];
    }

    module_load_include('inc', 'course_zoom', 'course_zoom.timezone');
    $time_zone_arr = course_zoom_timezones();

    $form['zoom']['zoom_info'] = array(
      '#weight' => -12,
      '#prefix' => '<div id="zoom-info-div">',
      '#suffix' => '</div>',
      '#title' => t('Zoom options'),
      '#type' => 'fieldset',
    );

    $form['zoom']['zoom_info']['video'] = array(
      '#weight' => -3,
      '#prefix' => '<div id="zoom-video-div">',
      '#suffix' => '</div>',
      '#title' => t('Video'),
      '#type' => 'fieldset',
    );

    $form['zoom']['zoom_info']['audio_fset'] = array(
      '#weight' => -2,
      '#prefix' => '<div id="zoom-audio-div">',
      '#suffix' => '</div>',
      '#title' => t('Audio'),
      '#type' => 'fieldset',
    );

    $form['zoom']['helptext_fset'] = array(
      '#weight' => -2,
      '#prefix' => '<div id="zoom-helptext-div">',
      '#title' => t('Help text'),
      '#description' => t('This will show above the launch link.'),
      '#suffix' => '</div>',
      '#type' => 'fieldset',
    );

    $form['zoom']['zoom_info']['date_fset'] = array(
      '#weight' => -10,
      '#prefix' => '<div id="zoom-date-div">',
      '#suffix' => '</div>',
      '#title' => t('Start date'),
      '#type' => 'fieldset',
      '#required' => TRUE,
    );

    $form['zoom']['zoom_info']['zoom_id_fset'] = array(
      '#weight' => -18,
      '#prefix' => '<div id="zoom-id-div">',
      '#suffix' => '</div>',
      '#type' => 'fieldset',
      '#title' => t('Zoom meeting ID'),
      '#attributes' => array('style' => 'display:none'),
    );

    $form['zoom']['zoom_info']['meeting_options'] = array(
      '#weight' => 1,
      '#prefix' => '<div id="zoom-meeting-options-div">',
      '#suffix' => '</div>',
      '#title' => t('Other options'),
      '#type' => 'fieldset',
    );

    $form['zoom']['zoom_info']['mode'] = array(
      '#type' => 'radios',
      '#title' => t('Type'),
      '#options' => ['meeting' => 'Meeting', 'webinar' => 'Webinar'],
      '#default_value' => $this->getOption('mode'),
      '#required' => TRUE,
      '#weight' => -25,
      '#ajax' => array(
        'callback' => 'zoom_form_callback',
        'wrapper' => 'zoom-info-div',
        'method' => 'replace',
      ),
    );

    $form['zoom']['zoom_info']['response'] = array(
      '#type' => 'select',
      '#title' => t('Select Zoom @mode source', ['@mode' => $mode]),
      '#options' => $meeting_topic_id_arr,
      '#default_value' => $this->getInstanceId(),
      '#required' => FALSE,
      '#weight' => -20,
      '#ajax' => array(
        'callback' => 'zoom_form_callback',
        'wrapper' => 'zoom-info-div',
        'method' => 'replace',
      ),
    );

    $form['zoom']['zoom_info']['zoom_id_fset']['instance'] = array(
      '#type' => 'textfield',
      '#description' => t('Enter the @mode ID, also known as the @mode number.', ['@mode' => $this->getMode()]),
      '#default_value' => $defaults['instance'],
      '#size' => 60,
      '#maxlength' => 128,
      '#weight' => -18,
      '#required' => FALSE,
    );

    $form['zoom']['zoom_info']['zoom_id_fset']['button'] = array(
      '#type' => 'button',
      '#default_value' => t('Get @mode', ['@mode' => $this->getMode()]),
      '#required' => FALSE,
      '#weight' => -15,
      '#limit_validation_errors' => array(),
      '#ajax' => array(
        'callback' => 'zoom_form_callback',
        'wrapper' => 'zoom-info-div',
        'method' => 'replace',
      ),
    );

    $form['zoom']['zoom_info']['topic'] = array(
      '#type' => 'textfield',
      '#title' => t('Topic'),
      '#default_value' => $defaults['topic'],
      '#description' => t(''),
      '#size' => 60,
      '#maxlength' => 128,
      '#weight' => -15,
      '#required' => TRUE,
    );

    $form['zoom']['zoom_info']['description'] = array(
      '#type' => 'textarea',
      '#title' => t('Description'),
      '#default_value' => $defaults['description'],
      '#weight' => -14,
      '#resizable' => FALSE,
    );

    $form['zoom']['helptext_fset']['helptext'] = array(
      '#type' => 'text_format',
      '#weight' => 100,
      '#default_value' => $this->getOption('helptext_value'),
      '#format' => $this->getOption('helptext_format') ?: filter_default_format(),
    );

    $form['zoom']['zoom_info']['required_attendance'] = array(
      '#type' => 'textfield',
      '#title' => t('Required attendance time'),
      '#description' => t('Required attendance in minutes. It is not recommended to enter more than 80 percent of the event time as required. For example, if your event is one hour long, enter 48 minutes.'),
      '#default_value' => $defaults['required_attendance'],
      '#element_validate' => array('element_validate_number'),
      '#weight' => -13,
      '#required' => TRUE,
    );

    $form['zoom']['zoom_info']['date_fset']['start_time'] = array(
      '#type' => 'date_popup',
      '#default_value' => $defaults['start_time'],
      '#date_format' => 'm/d/Y h:ia',
      '#weight' => -8,
      '#required' => TRUE,
    );

    $form['zoom']['zoom_info']['date_fset']['time_zone'] = array(
      '#type' => 'select',
      '#title' => t('Time zone'),
      '#options' => $time_zone_arr,
      '#default_value' => $defaults['time_zone'],
      '#required' => TRUE,
      '#weight' => -5,
    );

    $form['zoom']['zoom_info']['date_fset']['duration'] = array(
      '#type' => 'textfield',
      '#size' => 15,
      '#title' => t('Meeting duration'),
      '#description' => t('Length of meeting in minutes.'),
      '#default_value' => $defaults['duration'],
      '#element_validate' => array('element_validate_number'),
      '#weight' => -7,
      '#required' => TRUE,
    );

    $form['zoom']['zoom_info']['video']['host'] = array(
      '#type' => 'radios',
      '#title' => t('Host'),
      '#default_value' => $defaults['host'],
      '#options' => array('host_on' => t('On'), 'host_off' => t('Off')),
      '#description' => '',
      '#weight' => -14,
    );

    $form['zoom']['zoom_info']['video']['participant'] = array(
      '#type' => 'radios',
      '#title' => t('Participant'),
      '#default_value' => $defaults['participant'],
      '#options' => array('par_on' => t('On'), 'par_off' => t('Off')),
      '#description' => '',
      '#weight' => -14,
    );

    $audio_options = array('telephony' => t('Telephone'), 'voip' => t('Computer audio'), 'both' => t('Both'));

    $form['zoom']['zoom_info']['audio_fset']['audio'] = array(
      '#type' => 'radios',
      '#default_value' => $defaults['audio'],
      '#options' => $audio_options,
      '#description' => '',
      '#weight' => -2,
    );

    $recording_options = array('local' => t('On the local computer'), 'cloud' => t('In the cloud'), 'none' => t('None'));

    $form['zoom']['zoom_info']['meeting_options']['recording_options'] = array(
      '#title' => t('Recording options'),
      '#type' => 'radios',
      '#default_value' => $defaults['recording_options'],
      '#options' => $recording_options,
      '#description' => '',
      '#weight' => 5,
    );

    $form['zoom']['zoom_info']['meeting_options']['alternative_hosts'] = array(
      '#type' => 'textfield',
      '#title' => t('Alternative hosts'),
      '#description' => t('Enter multiple emails or IDs, separated by a comma.'),
      '#default_value' => $defaults['alternative_hosts'],
      '#weight' => 10,
      '#required' => FALSE,
    );

    if ($this->getMode() == 'webinar') {
      // Experimental - maybe redirect to next object? But might be access
      // denied...
      $form['zoom']['zoom_info']['meeting_options']['post_webinar_survey'] = array(
        '#type' => 'checkbox',
        '#title' => t('Redirect after webinar'),
        '#description' => t('Redirect the user back to the object page after the webinar.'),
        '#default_value' => $defaults['post_webinar_survey'],
        '#weight' => 11,
        '#required' => FALSE,
        '#access' => FALSE,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    parent::save();

    if ($this->getInstanceId() != '') {
      $id = $this->getInstanceId();
      $options = $this->getOptions();

      // Zoom requires a specific date format otherwise it defaults
      // to the current date/time.
      $start_time = format_date(strtotime($options['start_time']), 'custom', 'Y-m-d H:i:s');
      // format_date won't auto-add the seconds despite the custom format
      // so we add them manually.
      $start_time = str_replace(' ', 'T', $options['start_time']) . ':00';

      $host_val = ($options['host'] == 'host_on');
      $par_val = ($options['participant'] == 'par_on');

      if (!ctype_digit($options['duration'])) {
        $options['duration'] = 15;
      }

      $options_arr = array(
        'topic' => $options['topic'],
        'type' => $options['type'],
        'start_time' => $start_time,
        'timezone' => $options['time_zone'],
        'duration' => $options['duration'],
        'password' => $options['password'],
        'agenda' => $options['description'],
        'settings' => [
          'audio' => $options['audio'],
          'alternative_hosts' => $options['alternative_hosts'],
          'host_video' => $host_val,
          'participant_video' => $par_val,
          'panelist_video' => $par_val,
          'auto_recording' => $options['recording_options'],
          'approval_type' => 0,
        ],
      );

      if ($this->getOption('post_webinar_survey')) {
        $options_arr['settings']['post_webinar_survey'] = 1;
        $options_arr['settings']['survey_url'] = url($this->getUrl(), ['absolute' => TRUE]);
      }

      $zoomapi = new Zoom();
      $endpoint = "{$this->getMode()}s";
      $response = $zoomapi->patch($endpoint . '/' . $id, $options_arr);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function create() {
    $options = $this->getOptions();
    $zoomapi = new Zoom();

    $start_time = str_replace(' ', 'T', $options['start_time']);

    $host_val = ($options['host'] == 'host_on');
    $par_val = ($options['participant'] == 'par_on');

    $userId = variable_get('course_zoom_host_account');
    if ($userId == '') {
      global $user;
      $userId = $user->mail;
    }

    if (!ctype_digit($options['duration'])) {
      $options['duration'] = 15;
    }

    $options_arr = array(
      'topic' => $options['topic'],
      'type' => $options['type'],
      'start_time' => $start_time,
      'timezone' => $options['time_zone'],
      'duration' => $options['duration'],
      'password' => $options['password'],
      'agenda' => $options['description'],
      'settings' => [
        'audio' => $options['audio'],
        'alternative_hosts' => $options['alternative_hosts'],
        'host_video' => $host_val,
        'participant_video' => $par_val,
        'auto_recording' => $options['recording_options'],
        'approval_type' => 0,
      ],
    );

    if (!$options['topic'] == '') {
      $endpoint = "{$this->getMode()}s";
      $response_obj = $zoomapi->post('users/' . $userId . '/' . $endpoint, $options_arr);
      $this->setInstanceId($response_obj['id']);
    }
    parent::create();
  }

  /**
   * Override the default course object report.
   *
   * {@inheritdoc}
   */
  function getReport($key) {
    if ($key == 'default') {
      return array(
        'title' => 'Overview',
        'content' => views_embed_view('course_zoom_object_report', 'page', $this->getCourseNid(), $this->getId()),
      );
    }
  }

  /**
   * Watch for fulfillment from Zoom.
   *
   * {@inheritdoc}
   */
  public function hasPolling() {
    return TRUE;
  }

}
